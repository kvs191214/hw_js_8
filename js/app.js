console.log('1. Обчисліть кількість рядків з довжиною більше за 3 символи.');

const words = ["travel", "hello", "eat", "ski", "lift"];

let numberOfWords = (words.filter((word) => word.length > 3)).length;

console.log(numberOfWords);

console.log('2. Відфільтруйте масив, щоб отримати тільки об\'єкти зі sex "чоловіча".');

const users = [

    {
        name: 'Іван',
        age: 25,
        sex: 'чоловіча'
    },

    {
        name: 'Світлана',
        age: 21,
        sex: 'жіноча'
    },

    {
        name: 'Сергій',
        age: 32,
        sex: 'чоловіча'
    },

    {
        name: 'Татьяна',
        age: 21,
        sex: 'жіноча'
    },
];

let usersMale = users.filter((user) => user.sex === 'чоловіча');

console.log(usersMale);

console.log('3.  Реалізувати функцію фільтру масиву за вказаним типом даних.');

const test = ['hello', 'world', 23, '23', null];

function filterBy(arr, type) {
    return arr.filter((arrayElement) => typeof(arrayElement) !== type )
}

console.log(filterBy(test, 'string'));