<b>Теоретичні питання</b>

1. Опишіть своїми словами як працює метод forEach.

2. Які методи для роботи з масивом мутують існуючий масив, а які повертають новий масив? Наведіть приклади.

3. Як можна перевірити, що та чи інша змінна є масивом?

4. В яких випадках краще використовувати метод map(), а в яких forEach()?

****
<b>Відповіді :</b>

1. Метод `forEach` вуконує вказану функцію один раз для кожного елементу масиву.<br>
   <br>
2. <b>Мутуючі методи масивів:</b>
    - `push()` - використовується для додавання елементів до існуючого масиву. Приклад :<br>
      <br>
      `let usersName = ['Serhii', 'Roman', 'Ivan'];`<br>
      `users.push('Maria');`<br>
      `console.log(usersName); отримуємо: ['Serhii', 'Roman', 'Ivan', 'Maria']`<br>
      <br>

    - `unshift()` - використовується для додавання елементів до початку масиву і збільшує індекс кожного елемента на
      одиницю. Приклад:<br>
      <br>
      `let usersName = ['Serhii', 'Roman', 'Ivan'];`<br>
      `usersName.unshift('Maria');`<br>
      `console.log(usersName); отримуємо: ['Maria', 'Serhii', 'Roman', 'Ivan']`<br>
      <br>
    - `pop()` - використовується для видалення останнього елемента з масиву. Приклад:<br>
      <br>
      `let usersName = ['Maria', 'Serhii', 'Roman', 'Ivan'];`<br>
      `usersName.pop();`<br>
      `console.log(usersName); отримуємо: ['Maria', 'Serhii', 'Roman']`<br>
      <br>
    - `shift()` - він видаляє перший елемент з масиву. Цей метод зсуває всі елементи, зменшуючи індекс кожного елемента
      на
      одиницю. Приклад:<br>
      <br>
      `let usersName = ['Maria', 'Serhii', 'Roman', 'Ivan'];`<br>
      `usersName.shift();`<br>
      `console.log(usersName); отримуємо: ['Serhii', 'Roman', 'Ivan']`<br>
      <br>
    - `splice()` - використовується для зміни вмісту масиву шляхом видалення або заміни існуючих елементів та/або
      додавання
      нових елементів на місці. Приклад:<br>
      <br>
      `let usersName = ['Maria', 'Serhii', 'Roman', 'Ivan'];`<br>
      `usersName.splice(1, 2);`<br>
      `console.log(usersName); отримуємо: ['Maria', 'Ivan']`<br>
      <br>
      <b>Немутуючі методи масивів:</b><br>
      <br>
    - `slice()` - вирізає частину масиву і повертає поверхневу копію цієї частини у новий об'єкт масиву. Приклад:<br>
      <br>
      `let usersName = ['Maria', 'Serhii', 'Roman', 'Ivan'];`<br>
      `let usersMale = sersName.slise(1);`<br>
      `console.log(usersMale); отримуємо: ['Serhii', 'Roman', 'Ivan']`<br>
      <br>
    - `concat()` - використовується для об'єднання двох чи більше масивів в один. Приклад:<br>
      <br>
      `let usersMale = ['Serhii', 'Roman', 'Ivan'];`<br>
      `let usersFemale = ['Maria', 'Svitlana', 'Natalia'];`<br>
      `let usersName = usersMale.concat(usersFemale);`<br>
      `console.log(usersName); отримуємо: ['Serhii', 'Roman', 'Ivan', 'Maria', 'Svitlana', 'Natalia']`<br>
      <br>
    - `map()` - використовується для створення нового масиву з існуючого, застосовуючи функцію до кожного елемента
      першого масиву. Приклад: <br>
      <br>
      `let usersName = ['Maria', 'Serhii', 'Roman', 'Ivan'];`<br>
      `function toLowerCase(name) {
      return name.toLowerCase()
      }`<br>
      `let userNameLower = usersName.map(toLowerCase);`<br>
      `console.log(userNameLower); отримуємо: ['maria', 'serhii', 'roman', 'ivan']`<br>
      <br>
    - `filter()` - приймає кожен елемент масиву і застосовує до нього умовний оператор. Якщо умова true то додає
      значення у новий масив. Приклад:<br>
      <br>
      `let usersName = ['Maria', 'Serhii', 'Roman', 'Ivan'];`<br>
      `function maxNameLength(name) {
      return name.length <= 5;
      }`<br>
      `let shortNames = usersName.filter(maxNameLength);`<br>
      `console.log(shortNames); отримуємо: ['Maria', 'Roman', 'Ivan']`<br>
      <br>
    - `reduce()` - зменшує масив значень до одного значення. Приклад: <br>
      <br>
      `let usersAge = [21, 32, 28, 45];`<br>
      `function middleAge(usersAge) {
      return usersAge.reduce((total, age) => total + age, 0) / usersAge.length
      }`<br>
      `let usersMiddleAge = middleAge(usersAge);`<br>
      `console.log(usersMiddleAge); отримуємо: 31.5`<br>
      <br>
    - `sort()` - цей метод використовується для сортування масиву. Приклад: <br>
      <br>
      `let userNameLower = ['maria', 'serhii', 'roman', 'ivan'];`<br>
      `userNameLower.sort();` <br>
      `console.log(userNameLower); отримуємо: ['ivan', 'maria', 'roman', 'serhii']`<br>
      <br>
    - `reverse()` - використовується щоб змінити порядок елементів у масиві на зворотній. Приклад: <br>
      <br>
      `let userNameLower = ['maria', 'serhii', 'roman', 'ivan'];`<br>
      `userNameLower.sort().reverse();`<br>
      `console.log(userNameLower); отримуємо: ['serhii', 'roman', 'maria', 'ivan']`<br>
      <br>
    - `every()` - використовується для перевірки того, чи всі елементи в масиві задовольняють певний тест, реалізований
      за допомогою наданої функції. Він повертає значення типу boolean, яке вказує, чи всі елементи відповідають
      зазначеній умові. Приклад: <br>
      <br>
      `let usersAge = [21, 32, 28, 45];` <br>
      `let isAdult = usersAge.every((age) => age >= 18 );` <br>
      `console.log(isAdult); отримуємо: true` <br>
      <br>
    - `some()` - використовується для перевірки, чи хоча б один елемент масиву задовольняє певний умовний критерій. Він
      повертає `true`, якщо хоча б один елемент відповідає умові, і `false`, якщо жоден елемент не задовольняє умові.
      Приклад: <br>
      <br>
      `let usersAge = [21, 32, 28, 45];` <br>
      `let isJunior = usersAge.some((age) => age < 18 );`<br>
      `console.log(isJunior); отримуємо: false`<br>
      <br>
    - `includes()` - використовується для перевірки того, чи масив містить конкретний елемент. Він повертає значення
      типу boolean, що вказує, чи зазначений елемент присутній в масиві чи ні. Приклад:<br>
      <br>
      `let userNameLower = ['maria', 'serhii', 'roman', 'ivan'];`<br>
      `let hasIvan = userNameLower.includes('ivan');`<br>
      `console.log(hasIvan); отримаємо: true`<br>
      <br>
    - `find()` - використовуються для знаходження першого та останнього елементів відповідно, які задовольняють певний
      умовний критерій в масиві. Приклад:<br>
      <br>
      `let userNameLower = ['maria', 'serhii', 'roman', 'ivan'];` <br>
      `let foundSerhii = userNameLower.find((name) => name === 'serhii');`<br>
      `console.log(foundSerhii); отримуємо: 'serhii'`<br>
      <br>
    - `indexOf()` - використовується для знаходження індексу першого входження елемента в масиві. Якщо елемент не
      знайдено, метод повертає -1. Приклад:<br>
      <br>
      `let userNameLower = ['maria', 'serhii', 'roman', 'ivan'];`<br>
      `let indexMaria = userNameLower.indexOf('maria');` <br>
      `console.log(indexMaria); отримаємо: 0`<br>
      <br>
    - `findIndex()` - використовується для знаходження індексу першого елемента в масиві, який задовольняє певний
      умовний критерій. Якщо елемент не знайдено, метод повертає -1. Приклад:<br>
      <br>
      `let userNameLower = ['maria', 'serhii', 'roman', 'ivan'];`<br>
      `let indexNameBeginsR = userNameLower.findIndex((name) => name[0] === 'r');`<br>
      `console.log(indexNameBeginsR); отримаємо: 2`<br>
      <br>
3. Щоб перевірити чи є змінна масивом треба використовувати функцію 'Array.isArray()'. Приклад: <br>
   <br>
   `let usersName = ['Maria', 'Serhii', 'Roman', 'Ivan'];`<br>
   `console.log(Array.isArray(usersName)); отримаємо: true`<br>
   <br>
4. У випадку якщо треба транформувати елементи масиву та створити новий список, не змінюючи вихідний масив треба
   використовувати метод `map()`.<br>
У випадку якщо треба виконати дію безпосередньо у поточному масиві використовують метод `forEach()`.